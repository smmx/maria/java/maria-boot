/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.app.errors;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppSetupException extends Exception {

    public AppSetupException(String msg) {
        super(msg);
    }

    public AppSetupException(String message, Throwable cause) {
        super(message, cause);
    }
}
