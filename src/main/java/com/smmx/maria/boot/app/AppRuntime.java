/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Path;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppRuntime {

    // SINGLETON
    private static AppRuntime INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AppRuntime getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppRuntime();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AppRuntime.class);

    // SEQUENCE
    public static void fail(int code, String reason) {
        LOGGER.error("Application has failed: {}.", reason);
        exit(code);
    }

    public static void fail(int code, Throwable ex) {
        LOGGER.error("Application has failed.", ex);
        exit(code);
    }

    public static void exit(int code) {
        LOGGER.info("Exiting with exit code {}.", code);
        System.exit(code);
    }

    // HOME
    public static Path home() {
        return AppRuntime.getInstance().getHomeDirectory();
    }

    public static File hfile(String path) {
        return AppRuntime.getInstance().getHomeDirectory()
            .resolve(path)
            .toFile();
    }

    // STATIC
    public static final int APP_CRASH_EXIT_CODE = 900;

    // MEMBER
    private App app;
    private Path home_directory;

    private AppRuntime() {
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;

        // LOG
        LOGGER.info("App: {} - {}.", app.getName(), app.getDescription());
    }

    public Path getHomeDirectory() {
        return home_directory;
    }

    public void setHomeDirectory(Path home_directory) {
        this.home_directory = home_directory;

        // LOG
        LOGGER.info("Home directory: {}.", home_directory);
    }

}
