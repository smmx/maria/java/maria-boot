/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.configuration;

import com.smmx.maria.boot.MariaBootException;
import com.smmx.maria.boot.app.AppRuntime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class Configuration extends HashMap<String, Object> {

    // SINGLETON
    private static Configuration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static Configuration getInstance() {
        if (INSTANCE == null) {
            try {
                INSTANCE = new Configuration();
            } catch (IOException ex) {
                throw new MariaBootException(ex);
            }
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(Configuration.class);

    // MEMBER
    private final String version;
    private final String author;
    private final Date timestamp;

    private Configuration() throws IOException {
        Map<String, Object> configuration = read();

        // LOAD
        this.putAll(configuration);

        // DATA
        this.version = dp()
            .require("version")
            .asString()
            .notNull()
            .apply(this);

        this.author = dp()
            .require("author")
            .asString()
            .notNull()
            .apply(this);

        this.timestamp = dp()
            .require("timestamp")
            .asDate()
            .notNull()
            .apply(this);
    }

    // LOAD
    private Map<String, Object> read() throws IOException {
        File file = AppRuntime.home().resolve("config/app.yaml").toFile();

        // READER
        FileReader reader = new FileReader(file);

        // RETURN
        return Collections.unmodifiableMap(new Yaml().loadAs(reader, Map.class));
    }

    // GETTERS
    public String getVersion() {
        return version;
    }

    public String getAuthor() {
        return author;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    // METHODS
    public void log() {
        LOGGER.info(
            "Config: Version={}, Author={}, Timestamp={}",
            Configuration.getInstance().getVersion(),
            Configuration.getInstance().getAuthor(),
            Configuration.getInstance().getTimestamp()
        );
    }
}
