/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.configuration;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppConfigurationException extends RuntimeException {

    public AppConfigurationException(String msg) {
        super(msg);
    }

}
