/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.configuration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppConfiguration {

    // SINGLETON
    private static AppConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AppConfiguration getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppConfiguration();
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfiguration.class);

    // MEMBER
    private final String app_uuid;
    private final String app_name;

    private AppConfiguration() {
        Map<String, Object> app_section = dp()
            .require("app")
            .asDictionary()
            .notNull()
            .apply(Configuration.getInstance());

        this.app_uuid = dp()
            .require("uuid")
            .asString()
            .notNull()
            .apply(app_section);

        this.app_name = dp()
            .require("name")
            .asString()
            .notNull()
            .apply(app_section);
    }

    // GETTERS
    public String getUUID() {
        return app_uuid;
    }

    public String getName() {
        return app_name;
    }

    // METHODS
    public void log() {
        LOGGER.info(
            "Instance: Name={}, UUID={}",
            AppConfiguration.getInstance().getName(),
            AppConfiguration.getInstance().getUUID()
        );
    }

}
