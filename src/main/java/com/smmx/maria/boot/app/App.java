/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.app;

import com.smmx.maria.boot.app.errors.AppConfigurationException;
import com.smmx.maria.boot.app.errors.AppSetupException;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class App {

    private final String name;
    private final String description;

    public App(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    // SEQUENCE
    public abstract void configureArgumentParser(ArgumentParser parser);

    public abstract void configure(Map<String, Object> arguments) throws AppConfigurationException;

    public abstract void setup() throws AppSetupException;

    public abstract void start();

    public abstract void stop();
}
