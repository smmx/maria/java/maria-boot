/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.app;

import com.smmx.maria.boot.app.errors.AppConfigurationException;
import com.smmx.maria.boot.app.errors.AppSetupException;
import net.sourceforge.argparse4j.inf.ArgumentParser;

import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public abstract class AppModule {

    private final String name;

    public AppModule(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void configureArgumentParser(ArgumentParser parser);

    public abstract void configure(Map<String, Object> args) throws AppConfigurationException;

    public abstract void setup() throws AppSetupException;

}
