/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot.app;

import com.smmx.maria.boot.app.errors.AppConfigurationException;
import com.smmx.maria.boot.app.errors.AppSetupException;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author omiguelc
 */
public abstract class ModularApp extends App {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(ModularApp.class);

    // MEMBER
    private final List<AppModule> modules;

    public ModularApp(String name, String description) {
        super(name, description);

        // INIT
        this.modules = new ArrayList();
    }

    public void addModule(AppModule module) {
        this.modules.add(module);
    }

    @Override
    public void configureArgumentParser(ArgumentParser parser) {
        modules.forEach(module -> {
            module.configureArgumentParser(parser);
        });
    }

    @Override
    public void configure(Map<String, Object> args) throws AppConfigurationException {
        for (AppModule mod : this.modules) {
            // LOG
            LOGGER.info("Configuring: {}", mod.getName());

            // CONFIGURE
            mod.configure(args);
        }
    }

    @Override
    public void setup() throws AppSetupException {
        for (AppModule mod : this.modules) {
            // LOG
            LOGGER.info("Setting up: {}", mod.getName());

            // SETUP
            mod.setup();
        }
    }

}
