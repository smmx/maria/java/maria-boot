/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.boot;

/**
 * @author Osvaldo Miguel Colin
 */
public class MariaBootException extends RuntimeException {

    public MariaBootException(String msg) {
        super(msg);
    }

    public MariaBootException(Throwable thrwbl) {
        super(thrwbl);
    }

    public MariaBootException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

}
